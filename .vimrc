"{{{Author: Juan Donosa
"}}}

"{{{=> General
execute pathogen#infect()

" Airline Theme Settings
let g:airline_theme='wombat'
let g:airline_powerline_fonts = 1

let maplocalleader = ","
let mapleader = "," "Fast saving using leaders (<leader>w)
let g:mapleader = ","

set autoindent
set smartindent

nmap <leader>w :w!<cr>
nmap <leader>q :q!<cr>
nmap <leader>W :wq!<cr>

"keep search matches in the middle of the window
nnoremap n nzzzv
nnoremap N Nzzzv

"copy/pasate from X11 clipboard
"http://vim.wikia/com/wiki/GNU/Linux_clipboard_copy/paste_with_xclip
vmap <leader>xyy :!xclip -f -sel clip<CR>
map <leader>xpp mz:-1r !xclip -o -sel clip<CR>`z

"reload vimrc manually
map <leader>reload :source ~/.vimrc<CR>

"Force sudo permission
map <leader>sudo :w !sudo tee % <CR><CR>

" make commands easier
map ; :

" quickly go between code blocks; example { code }
map <tab> %

" Disables automatic commenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" C-T for new tab
nnoremap <C-t> :tabnew<CR>

"Split opens bottom & right
set splitbelow splitright

"shortcuts for split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"Automatically deletes trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

"}}}

"{{{=>VIM Configuration
scriptencoding utf-8
set encoding=utf-8
set nocompatible
syntax on
filetype plugin indent on
set ignorecase
set hlsearch
set incsearch
set title

" Change case
inoremap <C-u> <esc>mzgUiw`za

"}}}

"{{{=> Vim-Plug
"Specify a directory for plugins
"-Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
Plug 'vimwiki/vimwiki'
Plug 'suan/vim-instant-markdown'
Plug 'altercation/vim-colors-solarized'
Plug 'vim-scripts/SQLComplete.vim'
Plug 'vim-scripts/Ranger.vim'
Plug 'mboughaba/i3config.vim'
Plug 'junegunn/goyo.vim'
call plug#end()
"}}}

"{{{=>VimWiki
"VimWiki - Personal Wiki for Vim
"https://github.com/vimwiki/vimwiki
let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
"}}}

"{{{=>Vim-Instant-Markdown
"Instant Markdown preview for Vim
"https://github.com/suan/vim-instant-markdown
let g:instant_markdown_autostart = 0 "disable autostart
map <leader>md :InstantMarkdownPreview<CR>
"}}}

"{{{=> Folding

nnoremap <leader>z za

set foldenable "enable folding
set foldmethod=marker "folds with markers not indentation
set foldlevel=0 "everything folded at default
"}}}

"{{{=> VIM Interface

set relativenumber
set number "Set line numbers

set showmode "Show what mode you're currently editing in

set nobackup "Do not backup files
set noswapfile "Do not keep swap

set autoread "Set to auto read when a file is changed from the outside

set ignorecase "Ignore case when searching

set incsearch "search as characters are entered

set hlsearch "highlight matches
"gets rid of highlighting
noremap <leader><space> :nohlsearch<CR>

set showmatch "Show matching brackets when text indicator is over them

"Spell-check set to F6
map <F6> :setlocal spell! spellang=en_us<CR>

"make p in Visual mode replace selected text with the yank register
vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>

set wildmenu "enables wildmenu
set wildmode=longest,list,full

set lazyredraw "redraw only when needed

set clipboard=unnamedplus "yanks to OS clipboard (use p/P to paste to vim from clipboard)
map <leader><leader><y> :call system("xclip -selection clipboard", @")
"}}}

"{{{"=> Colors and Fonts

syntax enable "Enable Syntax Highlighting
"}}}

"{{{"=> Movement

"Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /
"map <1-space> ? "currently not working because Ctrl does not map in Vim anymore?

"Remap j and k to act the same when in long wrapped lines
nnoremap j gj
nnoremap k gk
nnoremap <up> gk
nnoremap <down> gj
"}}}

"{{{=> Extra Commands

"Make presentaiton in Sent
"map<F8> :w!<CR>:!sent <c-r>%<CR><CR>
"Prawn CLI
map <F8> :exec '!nohup mpv ' . shellescape(getline('.'), 1) . ' >/dev/null 2>&1&'<CR><CR>

"Brings up URLview
noremap <leader>u :w<Home>silent <End> !urlview<CR>

" Vim-latex rules
let g:vimtex_toc_hide_help = 1
let g:vimtex_latexmk_enabled =1
let g:vimtex_disable_version_warning = 1
let g:vimtex_compiler_latexmk = {'callback' : 0}
let g:vimtex_view_method = 'mupdf'

"Goyo plugin makes text more readeable
map <leader>f :Goyo \| set linebreak<CR>

"Enable Goyo by default when using (neo)mutt
autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo

"Compile documents, using compiler script
map <leader>c :w! \| !compiler <c-r>%<CR><CR>

"Open corresponding .pdf/.html or preview using opout script
map <leader>p :!opout <c-r>%<CR><CR>

"Run xrdb whenever .Xdefaults or .Xresources are updated
autocmd BufWritePost ~/.Xresources, ~/.Xdefaults !xrdb %

"Navigating with guides
inoremap <Space><Tab> <Esc>/<++><Enter>"_c4l
vnoremap <Space><Tab> <Esc>/<++><Enter>"_c4l
map <Space><Tab> <Esc>/<++><Enter>"_c4l
"}}}

"{{{=> Japanese Input

"<C-j> to enter kana mode while in Insert mode
"<C-x><C-t> to bring up thesaurus completion

let g:skk_initial_mode='hira'
let g:skk_script = '~/.vim/bundle/skk.vim/plugin/skk.vim'

set completeopt=menuone,preview
set thesaurus=~/.vim/bundle/skk.vim/skk-jisyo-utf-8.l

function! ToggleMode()
	if 'hira' ==# g:skk_initial_mode
		let g:skk_initial_mode = 'kata'
	else
		let g:skk_initial_mode = 'hira'
	endif
execute "source " . g:skk_script
endfunction

so ~/.vim/bundle/skk.vim/plugin/skk.vim
nm <leader><leader>j :call ToggleMode()<CR>
imap <leader><leader>j <esc>:call ToggleMode()<CR>a
"}}}

"{{{=> Snippets
"""HTML
	autocmd FileType html inoremap ,b <b></b><Space><++><Esc>FbT>i
	autocmd FileType html inoremap ,it <em></em><Space><++><Esc>FeT>i
	autocmd FileType html inoremap ,1 <h1></h1><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,2 <h2></h2><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,3 <h3></h3><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,p <p></p><Enter><Enter><++><Esc>02kf>a
	autocmd FileType html inoremap ,a <a<Space>href=""><++></a><Space><++><Esc>14hi
	autocmd FileType html inoremap ,e <a<Space>target="_blank"<Space>href=""><++></a><Space><++><Esc>14hi
	autocmd FileType html inoremap ,ul <ul><Enter><li></li><Enter></ul><Enter><Enter><++><Esc>03kf<i
	autocmd FileType html inoremap ,li <Esc>o<li></li><Esc>F>a
	autocmd FileType html inoremap ,ol <ol><Enter><li></li><Enter></ol><Enter><Enter><++><Esc>03kf<i
	autocmd FileType html inoremap ,im <img src="" alt="<++>"><++><esc>Fcf"a
	autocmd FileType html inoremap ,td <td></td><++><Esc>Fdcit
	autocmd FileType html inoremap ,tr <tr></tr><Enter><++><Esc>kf<i
	autocmd FileType html inoremap ,th <th></th><++><Esc>Fhcit
	autocmd FileType html inoremap ,tab <table><Enter></table><Esc>O
	autocmd FileType html inoremap ,gr <font color="green"></font><Esc>F>a
	autocmd FileType html inoremap ,rd <font color="red"></font><Esc>F>a
	autocmd FileType html inoremap ,yl <font color="yellow"></font><Esc>F>a
	autocmd FileType html inoremap ,dt <dt></dt><Enter><dd><++></dd><Enter><++><esc>2kcit
	autocmd FileType html inoremap ,dl <dl><Enter><Enter></dl><enter><enter><++><esc>3kcc
	autocmd FileType html inoremap &<space> &amp;<space>
	autocmd FileType html inoremap á &aacute;
	autocmd FileType html inoremap é &eacute;
	autocmd FileType html inoremap í &iacute;
	autocmd FileType html inoremap ó &oacute;
	autocmd FileType html inoremap ú &uacute;
	autocmd FileType html inoremap ä &auml;
	autocmd FileType html inoremap ë &euml;
	autocmd FileType html inoremap ï &iuml;
	autocmd FileType html inoremap ö &ouml;
	autocmd FileType html inoremap ü &uuml;
	autocmd FileType html inoremap ã &atilde;
	autocmd FileType html inoremap ẽ &etilde;
	autocmd FileType html inoremap ĩ &itilde;
	autocmd FileType html inoremap õ &otilde;
	autocmd FileType html inoremap ũ &utilde;
	autocmd FileType html inoremap ñ &ntilde;
	autocmd FileType html inoremap à &agrave;
	autocmd FileType html inoremap è &egrave;
	autocmd FileType html inoremap ì &igrave;
	autocmd FileType html inoremap ò &ograve;
	autocmd FileType html inoremap ù &ugrave;
"MARKDOWN
	autocmd Filetype markdown,rmd map <leader>w yiWi[<esc>Ea](<esc>pa)
	autocmd Filetype markdown,rmd inoremap ,n ---<Enter><Enter>
	autocmd Filetype markdown,rmd inoremap ,b ****<++><Esc>F*hi
	autocmd Filetype markdown,rmd inoremap ,s ~~~~<++><Esc>F~hi
	autocmd Filetype markdown,rmd inoremap ,e **<++><Esc>F*i
	autocmd Filetype markdown,rmd inoremap ,h ====<Space><++><Esc>F=hi
	autocmd Filetype markdown,rmd inoremap ,i ![](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd inoremap ,a [](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd inoremap ,1 #<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd inoremap ,2 ##<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd inoremap ,3 ###<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd inoremap ,l --------<Enter>
	autocmd Filetype rmd inoremap ,r ```{r}<CR>```<CR><CR><esc>2kO
	autocmd Filetype rmd inoremap ,p ```{python}<CR>```<CR><CR><esc>2kO
	autocmd Filetype rmd inoremap ,c ```<cr>```<cr><cr><esc>2kO

"}}}
