#!/bin/bash
scrot /tmp/screen.png
convert /tmp/screen.png -scale 10% -scale 1000% /tmp/screen.png
[[ -f ~/.config/polybar/lock.png ]] && convert /tmp/screen.png -swirl 360 ~/.config/polybar/lock.png -gravity center -composite -matte /tmp/screen.png
mocp -P
i3lock -b -e -i /tmp/screen.png

